import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Company extends Observable {
    private List<Employee> employees;

    public Company() {
        employees = new ArrayList<Employee>();
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
        // register observer
    }

    public void removeEmployee(Employee employee) {
        employees.remove(employee);
        //de-register observer
    }

    public void setEmployeeSalary(Employee employee, double salary) {
        employee.setSalary(salary);
        setChanged();
        // notify the observer
    }

    public static void main(String[] args) {
        // Create a company
        Company company = new Company();

        // Add a manager and an engineer to the company
        Employee manager = new Manager("John Doe", 50000, "Marketing");
        Employee engineer = new Engineer("Jane Smith", 40000);
        company.addEmployee(manager);
        company.addEmployee(engineer);

        // Print employee details
        manager.getEmployeeDetails();
        System.out.println();
        engineer.getEmployeeDetails();
        System.out.println();

        // Give a bonus to the manager
        SalaryBonusDecorator bonusManager = new SalaryBonusDecorator(manager, 10);
        // print the bonus
        System.out.println();

        // Give a bonus to the engineer
        SalaryBonusDecorator bonusEngineer = new SalaryBonusDecorator(engineer, 5);
        // print the bonus
        System.out.println();

        // Change the salary of an employee
        company.setEmployeeSalary(manager, 55000);
        // HRManager is notified
        System.out.println();

        // Print employee details again
        manager.getEmployeeDetails();
        System.out.println();
        engineer.getEmployeeDetails();
        System.out.println();
    }
}